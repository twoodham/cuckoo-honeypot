# cuckoo-honeypot

Testing cuckoo application as a wrapper/honeypot for specific applications.

## Why cuckoo?

Cuckoo is an open-source malware analysis engine. It can be effectively used as a honeypot, which makes it interesting to play with as a potential defense-in-depth strategy for protecting production systems.

### What can it do?

As noted on the (project's website)[https://cuckoosandbox.org/]:

> Analyze many different malicious files (executables, office documents, pdf files, emails, etc) as well as malicious websites under Windows, Linux, macOS, and Android virtualized environments.

> Trace API calls and general behavior of the file and distill this into high level information and signatures comprehensible by anyone.
Dump and analyze network traffic, even when encrypted with SSL/TLS. With native network routing support to drop all traffic or route it through 

> InetSIM, a network interface, or a VPN.
Perform advanced memory analysis of the infected virtualized system through Volatility as well as on a process memory granularity using YARA.

## Experiments/TODOs

- [ ] Get cuckoo working within a docker container.
- [ ] Expose a DBMS (postgres?) and expose its API to the world.
- [ ] Get a Rails/django/etc app running inside the container and expose its API to the world.
- [ ] Discover the types of activities that may report data from the cuckoo container.